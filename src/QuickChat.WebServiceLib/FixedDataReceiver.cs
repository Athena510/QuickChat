﻿using Newtonsoft.Json;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    public class FixedDataReceiver : IDataReceiver
    {
        public string FilePath { get; set; }

        public FixedDataReceiver(string filePath)
        {
            FilePath = filePath;
        }
        
        public List<Message> RetrieveNewMessages(Conversation conversationToGetInformationFrom, DateTime timeStamp)
        {
            // Open the file to read from.
            string inputData = File.ReadAllText(FilePath);
            Console.WriteLine("Deserializing\n\n" + conversationToGetInformationFrom);
            return JsonConvert.DeserializeObject<List<Message>>(inputData);
        }

        public List<User> RetrieveUserInformation()
        {
            // Open the file to read from.
            string inputData = File.ReadAllText(FilePath);
            Console.WriteLine("Deserializing all user information\n\n");
            return JsonConvert.DeserializeObject<List<User>>(inputData);
        }

        public Conversation RetrieveConversation(Guid conversationToGet)
        {
            // Open the file to read from.
            string inputData = File.ReadAllText(FilePath);
            Console.WriteLine("Deserializing\n\n" + conversationToGet);
            return JsonConvert.DeserializeObject<Conversation>(inputData);
        }
    }
}
