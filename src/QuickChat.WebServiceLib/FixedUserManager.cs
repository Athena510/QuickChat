﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    class FixedUserManager : IUserManager
    {
        Dictionary<Guid, User> _allUsersByID;

        public FixedUserManager(List<User> allUsers)
        {
            _allUsersByID = new Dictionary<Guid, User>();

            foreach (User thisUser in allUsers)
            {
                _allUsersByID.Add(thisUser.UserID, thisUser);
            }
        }

        public User GetUserFromID(Guid userID)
        {
            return _allUsersByID[userID];
        }

        public Guid CreateNewUser(string firstName, string middleName, string lastName)
        {
            User newUser = new User(lastName, firstName, middleName, new Guid(), true, new List<Guid>());
            _allUsersByID.Add(newUser.UserID, newUser);

            return newUser.UserID;
        }

        public bool DeleteUser(Guid userToDelete, IConversationManager conversationManager)
        {
            conversationManager.RemoveUserFromAllConversations(userToDelete);
            return _allUsersByID.Remove(userToDelete);
        }

        public List<Guid> GetConversationIdsUserIsIn(Guid userToGetInfoFor)
        {
           return _allUsersByID[userToGetInfoFor].Conversations;
        }
    }
}
