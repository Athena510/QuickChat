﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    public interface IGlobalConfiguration
    {
        CultureInfo GetCultureInfo(); 
    }
}
