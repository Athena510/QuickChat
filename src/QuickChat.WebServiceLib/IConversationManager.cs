﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickChat.WebServiceLib
{
    public interface IConversationManager
    {
        void AddNewConversation(Conversation newConversationToAdd);

        bool DeleteConversation(Guid conversationIDToDelete, IUserManager userManager);

        void AddMessageToConversation(string message, Guid conversationID, Guid fromUserID);

        void RemoveUserFromConversation(Guid conversationID, Guid userID);

        void RemoveUserFromAllConversations(Guid userID);

        Conversation GetConversationFromID(Guid conversationToGet);
    }
}
