﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quickchat.UI
{
    /// <summary>
    /// Order matters in t
    /// </summary>
    public enum ContactStatus
    {
        Offline,
        Away,
        DoNotDisturb,
        Online
    }
}
