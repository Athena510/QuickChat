﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Quickchat.Server.Models;
using QuickChat.WebServiceLib;

namespace Quickchat.Server.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        public ValuesController(MessagesContext context)
        {
            _context = context;
        }


        private readonly MessagesContext _context;
        // GET api/values
        [HttpGet]
        public IEnumerable<Message> Get()
        {
            return _context.Messages;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Message Get(int id)
        {
            return _context.Messages.ToList<Message>()[id];
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Message value)
        {
            _context.Add(value);
            _context.SaveChanges();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
